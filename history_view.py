from Tkinter import *
from file_io import write_data
import datetime

now = datetime.datetime.now()
has_changed = False

def show(data_ls):

    def view():
        pass

    def handle_close():
        gui.quit()

    def delete():
        if lb_history.curselection():
            selection = int(lb_history.curselection()[0])
        else:
            return
        data_ls.pop(selection)
        lb_history.delete(selection)
        write_data(data_ls)
        global has_changed
        has_changed = True

    gui = Toplevel()
    gui.title("Transaction History")
    gui.geometry("575x220+200+200")
    gui.protocol("WM_DELETE_WINDOW", handle_close)

    # Listbox
    lb_history = Listbox(gui, width=70)

    for transaction in data_ls:
        lb_history.insert(END, transaction['date'] + " | " +
                            transaction['note'])
    lb_history.grid(column=0, row=0, columnspan=2, padx=5, pady=10)

    # DELETE button
    btn_delete = Button(gui, text="Delete", width=13, command=delete)
    btn_delete.grid(column=1, row=1, padx=5, pady=(0, 10), sticky=W)

    # VIEW button
    btn_view = Button(gui, text="View", width=13, command=view)
    btn_view.grid(column=1, row=1, padx=5, pady=(0, 10), sticky=E)

    # MONTH TOTAL
    str_month_total = StringVar()
    lbl_month_total = Label(gui, textvariable=str_month_total, height=1)
    lbl_month_total.grid(column=0, row=1, padx=5, pady=(0, 10), sticky=W)
    str_month_total.set("$" + str(calc_month_total(data_ls)))

    gui.mainloop()

    gui.destroy()

    global has_changed
    return has_changed

def calc_month_total(data_ls):
    total = 0
    for transaction in data_ls:
        if current_month(transaction['date']):
            total += transaction['cost']
    return total

def current_month(date):
    month = date.split('-')[0]
    return int(month) == now.month
