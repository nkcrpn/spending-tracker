#! /usr/bin/python

from Tkinter import *
from file_io import *
import tkMessageBox
import datetime
import json
import history_view

now = datetime.datetime.now()

transaction_ls = load_transactions()
# TODO I don't like this implementation
# not at all...
has_changed = False

def submit_data():
    note = txt_note.get(1.0, END).rstrip('\n')
    type_ = type_text.get().rstrip(' \n')
    date = tn_date.get().rstrip('\n')
    try:
        cost = tn_cost.get()
    except ValueError:
        error_message()
        return

    if valid_note(note) and valid_cost(cost) and valid_date(date):
        transaction_ls.append({'note':note, 'type':type_, 'date':date,
            'cost':cost})
        write_data(transaction_ls)
        success_message()
        clear_fields()
        global has_changed
        has_changed = True
    else:
        error_message()
        return

def clear_fields():
    txt_note.delete(1.0, END)
    type_text.set(types[0])
    ety_cost.delete(0, END)
    tn_date.set("%02d-%02d-%04d" %(now.month, now.day, now.year))

def success_message():
    header.set("Successfully saved!")

def valid_note(text):
    return len(text) > 0

def valid_cost(number):
    sig_fig = str(number).split('.')[1]
    return number > 0 and len(sig_fig) <= 2

def valid_date(text):
    return len(text) == 10 and text[2] == '-' and text[5] == '-'

def error_message():
    tkMessageBox.showerror("Invalid Input", "Something isn't right. Please "
                            "check your inputs.")

def view_history():
    if history_view.show(transaction_ls):
        global has_changed
        has_changed = True

def handle_close():
    global has_changed
    if has_changed:
        print 'Backing up data...'
        backup_data()
    gui.destroy()

gui = Tk()
gui.title("Save a Transaction")
gui.geometry("225x285+200+200")
gui.resizable(width=False, height=False)
gui.protocol("WM_DELETE_WINDOW", handle_close)

menu_bar = Menu(gui)

# FILE menu
mn_file = Menu(menu_bar, tearoff=0)
mn_file.add_command(label="View History", command=view_history)

# Separated options
mn_file.add_separator()
mn_file.add_command(label="Quit", command=gui.quit)
menu_bar.add_cascade(label="File", menu=mn_file)

gui.config(menu=menu_bar)

# App Header
header = StringVar()
header.set("Enter a transaction!")
lbl_header = Label(gui, textvariable=header, height=1)
lbl_header.grid(column=0, row=0, columnspan=2, pady=10)

# NOTESS frame
lf_note = LabelFrame(gui, text="Note", bd=0)
lf_note.grid(column=0, row=2, columnspan=2, padx=5, pady=10, sticky=N+S+W+E)

# NOTESS text
txt_note = Text(lf_note, width=30, height=2)
txt_note.grid(sticky=N+W+S+E)

# COST frame
lf_cost = LabelFrame(gui, text="Cost", bd=0)
lf_cost.grid(column=1, row=3, padx=5, pady=10)

# COST entry
tn_cost = DoubleVar()
ety_cost = Entry(lf_cost, textvariable=tn_cost, width=5)
ety_cost.grid()
ety_cost.delete(0, END)

# DATE frame
lf_date = LabelFrame(gui, text="Date", bd=0)
lf_date.grid(column=0, row=4, padx=5, pady=10)

# DATE entry
tn_date = StringVar()
tn_date.set("%02d-%02d-%04d" %(now.month, now.day, now.year))
ety_date = Entry(lf_date, textvariable=tn_date, width=9)
ety_date.grid()

# TYPE frame
lf_type = LabelFrame(gui, text="Type", bd=0)
lf_type.grid(column=0, row=3, padx=5, pady=10)

# Type optionMenu
types = [ "Rent", "Utilities", "Groceries", "Books" ]
type_text = StringVar()
type_text.set(types[0])
om_type = OptionMenu(lf_type, type_text, *types)
om_type.grid()

# SAVE button
btn_submit = Button(gui, text="Save", width=10, command=submit_data)
btn_submit.grid(column=0, row=5, columnspan=2, padx=5, pady=10)

gui.mainloop()
