import json
import os
import datetime

# TODO USE A DATABASE. How to persist?

TRANSACTION_FILE = '/home/nick/Dev/Projects/Python/SpendingTracker/transactions.txt'
TRANSACTION_BACKUP_LS = ["/home/nick/Documents/SpendingTracker/transactionsBackup1.txt",
                            "/home/nick/Documents/SpendingTracker/transactionsBackup2.txt",
                            "/home/nick/Documents/SpendingTracker/transactionsBackup3.txt"]

def load_transactions():
    try:
        transactions = json.loads(open(TRANSACTION_FILE, 'r').read())
    except ValueError, e:
        print "Could not load JSON"
        print e
        transactions = []
    return transactions

def write_data(data_ls):
    jdump = json.dumps(data_ls)
    with open(TRANSACTION_FILE, 'w') as my_file:
        my_file.write(jdump)

def backup_data():
    to_backup = json.dumps(load_transactions())
    with get_oldest_backup_file('w') as my_file:
        my_file.write(to_backup)

def get_oldest_backup_file(pref):
    oldest_file = TRANSACTION_BACKUP_LS[0]
    for next_file in TRANSACTION_BACKUP_LS[1:]:
        if modification_date(next_file) < modification_date(oldest_file):
            oldest_file = next_file

    # TODO perhaps opening should be done in backup_data
    # so as to make catching exceptions easier
    # this needs to be in a try/catch tbh...
    return open(oldest_file, pref)

def modification_date(filename):
    time = os.path.getmtime(filename)
    return datetime.datetime.fromtimestamp(time)
